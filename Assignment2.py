from flask import Flask, render_template, request, redirect, url_for, jsonify
import random

app = Flask(__name__) #instance of the flask class

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title':'Algorithm Design', 'id':'2'},
         {'title':'Python', 'id':'3'}]


@app.route('/', methods = ['GET','POST']) #ties the url to the homepage
def index():
    return render_template("showPage.html", books = books)

@app.route('/book/new/', methods = ['GET','POST'])
def new():
    if request.method == 'POST':
        search_key = request.form['name']
        books.append({"title": search_key, 'id': str((len(books) +1))})
        return redirect(url_for('index'))
    else:
        return render_template("newBook.html")

@app.route('/delete/<int:id>', methods = ['GET', 'POST'])
def delete(id):
    if request.method == 'POST':
        index = None
        for x in range(len(books)):
            if books[x]['id'] == str(id):
                index = x
        books.pop(index)
        for x in range(len(books)):
            books[x]['id'] = str(x+1)
        return redirect(url_for('index'))
    else:
        index = None
        for x in range(len(books)):
            if books[x]['id'] == str(id):
                index = x
        return render_template('deleteBook.html', books = books, id = id, book = books[index]['title'])


@app.route('/edit/<int:id>', methods = ['GET','POST']) #routes the url, /<int:id>/ is a dynamic way to choose a number
def edit(id):
    if request.method == 'POST':
        index = None
        for x in range(len(books)):
            if books[x]['id'] == str(id):
                index = x
        books[index]['title'] = request.form['name']
        return redirect(url_for('index'))
    else:
        index = None
        for x in range(len(books)):
            if books[x]['id'] == str(id):
                index = x
        return render_template('editBook.html', books = books, id = id, book = books[index]['title'])



if __name__ == "__main__":
    app.run()

